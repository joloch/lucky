<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('error_reporting', E_ALL);

    /**
     * Calculate the lucky numbers.
     */
    class GetLucky {
    
        /*
         * Required string length
         *
         * @var int
         */
        public $string_length = 6;
        
        /*
         * Start value 
         *
         * @var int
         */
        protected $first = 0;
        
        /*
         * End value
         *
         * @var int
         */
        protected $end = false;
        
        /*
         * Description of validation errors
         *
         * @var str
         */
        protected $error = '';
        
        /*
         * Main function. Get count of lucky numbers.
         * Data set in validate function.
         *
         * @return int
         */
        public function calculate() :int {
            
            if($this->first===false || $this->end===false) {
                return false;
            }//if
            $counter = 0;
            $half_str= ($this->string_length/2);
            do {
                //$parts = array_chunk( str_split(str_pad($first, 6, '0', STR_PAD_LEFT)), 3);

                $parts = array_chunk(array_reverse(str_split($this->first)), $half_str);
                $first_part = $this->getSum($parts[0]);
                $second_part= isset($parts[1]) ? $this->getSum($parts[1]) : 0;
                
                if($first_part == $second_part) $counter++;
                $this->first++;

            } while ($this->first <= $this->end);
            
            return $counter;
        }
        
        /*
         * Validation of incoming data.
         *
         * @param array $data ['first','end']
         * @return bool
         */
        public function validate (array $data) :bool {
            $validate = true;
            
            if(!isset($data['first'])) {
                $this->setError('Укажите значение в поле first');
                $validate = false;
            }//if
            
            if(!isset($data['end'])) {
                $this->setError('Укажите значение в поле end');
                $validate = false;
            }//if
            
            if($this->string_length != strlen($data['first'])) {
                $this->setError('Необходимая длинна для поля first - '.$this->string_length);
                $validate = false;
            }//if
            
            if($this->string_length != strlen($data['end'])) {
                $this->setError('Необходимая длинна для поля end - '.$this->string_length);
                $validate = false;
            }//if
            
            if(isset($data['first']) && isset($data['end']) && intval($data['first']) > intval($data['end'])) {
                $this->setError('first не может иметь подобное заниженое значение!)');
                $validate = false;
            }//if
            
            if($validate) {
                $this->first = intval($data['first']);
                $this->end   = intval($data['end']);
            }//if
            
            return $validate;
        }
        
        /*
         * Get errors
         */
        public function getError() {
            return $this->error;
        }
        
        /*
         * Set errors.
         */
        protected function setError(string $err) {
            $this->error .= $err.'<br>';
        }
        
        /*
         * Get sum of half number.
         *
         * @param array $number
         * @return int
         */
        protected function getSum(array $number) :int {
            $sum = array_sum($number);
            if(strlen($sum)>=2) {
                return $this->getSum(str_split($sum));
            }//if
            
            return $sum;
        }
        
    }

if(isset($_GET['first']) || isset($_GET['end'])) {
    $counter = new GetLucky();
    if(!$counter->validate($_GET)) {
        echo $counter->getError();
    } else {
        echo $counter->calculate();
    }//else
}else{
?>
<html>
<body>
    <form action="" method="get">
        <input type="text" name="first" placeholder="first">
        <input type="text" name="end" placeholder="end">
        <input type="submit" value="Посчитать количество счастливых билетиков!">
    </form>
</body>
</html>
<?php }

 
